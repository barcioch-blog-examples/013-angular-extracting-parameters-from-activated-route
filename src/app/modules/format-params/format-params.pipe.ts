import { Pipe, PipeTransform } from '@angular/core';
import { Params } from '@angular/router';

@Pipe({
  name: 'formatParams',
})
export class FormatParamsPipe implements PipeTransform {

  transform(input: Params): string {
    return Object.keys(input).map(key => `${key}:${input[key]}`).join(',');
  }
}
