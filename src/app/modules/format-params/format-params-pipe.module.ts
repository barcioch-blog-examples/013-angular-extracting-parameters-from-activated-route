import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatParamsPipe } from './format-params.pipe';

@NgModule({
  declarations: [FormatParamsPipe],
  exports: [FormatParamsPipe],
  imports: [
    CommonModule,
  ],
})
export class FormatParamsPipeModule {
}
