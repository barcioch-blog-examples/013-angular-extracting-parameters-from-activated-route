import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormatParamsPipeModule } from '../format-params/format-params-pipe.module';

@Component({
  selector: 'app-test',
  template: `
    <div data-test='observable-params'>{{ activatedRoute.params | async | formatParams }}</div>
    <div data-test='observable-query-params'>{{ activatedRoute.queryParams | async | formatParams }}</div>
    <div data-test='observable-data'>{{ activatedRoute.data | async | formatParams }}</div>

    <div data-test='snapshot-params'>{{ activatedRoute.snapshot?.params | formatParams }}</div>
    <div data-test='snapshot-query-params'>{{ activatedRoute.snapshot?.queryParams | formatParams }}</div>
    <div data-test='snapshot-data'>{{ activatedRoute.snapshot?.data | formatParams }}</div>

    <div data-test='initial-snapshot-params'>{{ initialReference?.params | formatParams }}</div>
    <div data-test='initial-snapshot-query-params'>{{ initialReference?.queryParams | formatParams }}</div>
    <div data-test='initial-snapshot-data'>{{ initialReference?.data | formatParams }}</div>
  `,
  standalone: true,
  imports: [CommonModule, FormatParamsPipeModule],
})
export class TestComponent {
  readonly activatedRoute = inject(ActivatedRoute);
  readonly initialReference = this.activatedRoute.snapshot;
}
