import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-snapshot-data',
  template: `
    <div>{{ activatedRoute.snapshot.params | formatParams }}</div>
    <div>{{ activatedRoute.snapshot.queryParams | formatParams }}</div>
    <div>{{ activatedRoute.snapshot.data | formatParams }}</div>
  `
})
export class SnapshotDataComponent implements OnInit {
  readonly activatedRoute = inject(ActivatedRoute);

  ngOnInit(): void {
    console.log(this.activatedRoute.snapshot.params);
    console.log(this.activatedRoute.snapshot.queryParams);
    console.log(this.activatedRoute.snapshot.data);
  }
}
