import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-observable-data',
  template: `
    <div>{{ activatedRoute.params | async | formatParams }}</div>
    <div>{{ activatedRoute.queryParams | async | formatParams }}</div>
    <div>{{ activatedRoute.data | async | formatParams }}</div>
  `
})
export class ObservableDataComponent implements OnInit {
  readonly activatedRoute = inject(ActivatedRoute);

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => console.log(params));
    this.activatedRoute.queryParams.subscribe((queryParams: Params) => console.log(queryParams));
    this.activatedRoute.data.subscribe((data: Params) => console.log(data));
  }
}
