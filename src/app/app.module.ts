import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterOutlet } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { SnapshotDataComponent } from './snapshot-data/snapshot-data.component';
import { FormatParamsPipeModule } from './modules/format-params/format-params-pipe.module';
import { ObservableDataComponent } from './observable-data/observable-data.component';

@NgModule({
  declarations: [
    AppComponent,
    SnapshotDataComponent,
    ObservableDataComponent
  ],
  imports: [
    BrowserModule,
    RouterOutlet,
    AppRoutingModule,
    FormatParamsPipeModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
