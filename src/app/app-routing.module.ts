import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { TestComponent } from './modules/test/test.component';
import { ParentsComponent } from './modules/parents/parents.component';


export const routes: Routes = [
  {
    path: ``,
    component: AppComponent,
    children: [
      {
        path: `parents`,
        children: [
          {
            path: '',
            data: {
              secret: 'secret-parents-data',
            },
            component: ParentsComponent,
          },
          {
            path: ':parentId',
            component: TestComponent,
            data: {
              secret: 'secret-parent-details-data',
            },
          }
        ]
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {paramsInheritanceStrategy: 'always'})],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
