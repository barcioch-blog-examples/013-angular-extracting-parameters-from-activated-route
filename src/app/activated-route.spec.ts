import { Router } from '@angular/router';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TestComponent } from './modules/test/test.component';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { Component } from '@angular/core';


describe('ActivatedRouteTest', () => {
  let router: Router;
  let fixture: ComponentFixture<RouterOutletComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [RouterOutletComponent],
      imports: [
        TestComponent,
        RouterTestingModule.withRoutes([
          {
            path: '',
            component: RouterOutletComponent,
            children: [
              {
                path: `products`,
                children: [
                  {
                    path: ':productId',
                    component: TestComponent,
                    data: {
                      secret: 'product-details',
                    },
                  },
                  {
                    path: '',
                    data: {
                      secret: 'product-list',
                    },
                    component: TestComponent,
                  },
                ]
              },
            ]
          }
        ], {paramsInheritanceStrategy: 'always'}),
      ],
    });

    fixture = TestBed.createComponent(RouterOutletComponent);
    router = TestBed.inject(Router);
    router.initialNavigation();
  });

  describe('when user enters /products url', () => {
    beforeEach(async () => {
      await router.navigateByUrl('/products');
      fixture.detectChanges();
      await fixture.whenStable();
    });

    it('should display route properties', () => {
      const expected: SnapshotDump = {
        params: '',
        queryParams: '',
        data: 'secret:product-list',
        snapshotParams: '',
        snapshotQueryParams: '',
        snapshotData: 'secret:product-list',
        initialSnapshotParams: '',
        initialSnapshotQueryParams: '',
        initialSnapshotData: 'secret:product-list',
      };
      expect(getSnapshotDump()).toEqual(expected);
    });

    describe('and user navigates to /products/123 url', () => {
      beforeEach(async () => {
        await router.navigateByUrl('/products/123');
        fixture.detectChanges();
      });

      it('should display route properties', () => {
        const expected: SnapshotDump = {
          params: 'productId:123',
          queryParams: '',
          data: 'secret:product-details',
          snapshotParams: 'productId:123',
          snapshotQueryParams: '',
          snapshotData: 'secret:product-details',
          initialSnapshotParams: 'productId:123',
          initialSnapshotQueryParams: '',
          initialSnapshotData: 'secret:product-details',
        };
        expect(getSnapshotDump()).toEqual(expected);
      });

      describe('and user navigates to /products/998 url', () => {
        beforeEach(async () => {
          await router.navigateByUrl('/products/998');
          fixture.detectChanges();
        });

        it('should display route properties', () => {
          const expected: SnapshotDump = {
            params: 'productId:998',
            queryParams: '',
            data: 'secret:product-details',
            snapshotParams: 'productId:998',
            snapshotQueryParams: '',
            snapshotData: 'secret:product-details',
            initialSnapshotParams: 'productId:123',
            initialSnapshotQueryParams: '',
            initialSnapshotData: 'secret:product-details',
          };
          expect(getSnapshotDump()).toEqual(expected);
        });

        describe('and user navigates to /products/998?param1=val1 url', () => {
          beforeEach(async () => {
            await router.navigateByUrl('/products/998?param1=val1');
            fixture.detectChanges();
          });

          it('should display route properties', () => {
            const expected: SnapshotDump = {
              params: 'productId:998',
              queryParams: 'param1:val1',
              data: 'secret:product-details',
              snapshotParams: 'productId:998',
              snapshotQueryParams: 'param1:val1',
              snapshotData: 'secret:product-details',
              initialSnapshotParams: 'productId:123',
              initialSnapshotQueryParams: '',
              initialSnapshotData: 'secret:product-details',
            };
            expect(getSnapshotDump()).toEqual(expected);
          });

          describe('and user navigates to /products/998?param2=val2 url', () => {
            beforeEach(async () => {
              await router.navigateByUrl('/products/998?param2=val2');
              fixture.detectChanges();
            });

            it('should display route properties', () => {
              const expected: SnapshotDump = {
                params: 'productId:998',
                queryParams: 'param2:val2',
                data: 'secret:product-details',
                snapshotParams: 'productId:998',
                snapshotQueryParams: 'param2:val2',
                snapshotData: 'secret:product-details',
                initialSnapshotParams: 'productId:123',
                initialSnapshotQueryParams: '',
                initialSnapshotData: 'secret:product-details',
              };
              expect(getSnapshotDump()).toEqual(expected);
            });
          });
        });
      });
    });
  });

  const getSnapshotDump = (): SnapshotDump => {
    return {
      params: fixture.debugElement.query(By.css('[data-test="observable-params"]')).nativeElement.textContent,
      queryParams: fixture.debugElement.query(By.css('[data-test="observable-query-params"]')).nativeElement.textContent,
      data: fixture.debugElement.query(By.css('[data-test="observable-data"]')).nativeElement.textContent,
      snapshotParams: fixture.debugElement.query(By.css('[data-test="snapshot-params"]')).nativeElement.textContent,
      snapshotQueryParams: fixture.debugElement.query(By.css('[data-test="snapshot-query-params"]')).nativeElement.textContent,
      snapshotData: fixture.debugElement.query(By.css('[data-test="snapshot-data"]')).nativeElement.textContent,
      initialSnapshotParams: fixture.debugElement.query(By.css('[data-test="initial-snapshot-params"]')).nativeElement.textContent,
      initialSnapshotQueryParams: fixture.debugElement.query(By.css('[data-test="initial-snapshot-query-params"]')).nativeElement.textContent,
      initialSnapshotData: fixture.debugElement.query(By.css('[data-test="initial-snapshot-data"]')).nativeElement.textContent,
    }
  }
});


@Component({
  selector: 'router-outlet-dummy',
  template: '<router-outlet></router-outlet>',
})
export class RouterOutletComponent {
}

interface SnapshotDump {
  params: string;
  queryParams: string;
  data: string;
  snapshotParams: string;
  snapshotQueryParams: string;
  snapshotData: string;
  initialSnapshotParams: string;
  initialSnapshotQueryParams: string;
  initialSnapshotData: string;
}
